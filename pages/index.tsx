import type { GetServerSidePropsResult, NextPage } from "next";

import { ToDo } from "../types/ToDo";
import { ToDoApiFactory } from "../api/ToDoApiFactory";
import { ToDoWidget } from "../components/ToDoWidget";

const toDoApi = ToDoApiFactory.get();

interface HomepageProps {
  toDos: ToDo[];
}

export async function getServerSideProps(): Promise<
  GetServerSidePropsResult<HomepageProps>
> {
  return {
    props: {
      toDos: await toDoApi.getAllToDos(),
    },
  };
}

const Home: NextPage<HomepageProps> = ({
  toDos: toDosFromProps,
}: HomepageProps) => {
  return <ToDoWidget initialToDos={toDosFromProps} toDoApi={toDoApi} />;
};

export default Home;
