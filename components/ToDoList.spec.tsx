import { act, fireEvent, render, RenderResult } from "@testing-library/react";

import { mockToDos } from "../lib-test/mock-todos";
import { ToDoList } from "./ToDoList";
import { ToDoNewFormProps } from "./ToDoNewForm";

describe("ToDoList", () => {
  let renderResult: RenderResult;
  let updateToDoDoneStatusMock = jest.fn();
  let addToDoMock: ToDoNewFormProps["onSubmit"];
  let input: HTMLInputElement;

  beforeEach(async () => {
    addToDoMock = jest.fn().mockImplementation((Name: string) => {
      const lastToDo = mockToDos[mockToDos.length - 1];
      mockToDos.push({ id: lastToDo.id++, Name, Done: false });
    });

    renderResult = render(
      <ToDoList
        toDos={mockToDos}
        updateToDoDoneStatus={updateToDoDoneStatusMock}
        addToDo={addToDoMock}
      />
    );
  });

  it("should render all to-dos", () => {
    expect(renderResult.container).toBeTruthy();
    for (const toDo of mockToDos) {
      expect(renderResult.getByText(toDo.Name ?? "")).toBeInTheDocument();
    }
  });

  it("should render a new todo after it has been submitted", async () => {
    const value = "my todo";

    act(() => {
      renderResult.rerender(
        <ToDoList
          toDos={mockToDos}
          updateToDoDoneStatus={updateToDoDoneStatusMock}
          addToDo={addToDoMock}
        />
      );
    });

    input = renderResult.container.querySelector(
      'input[name="name"]'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, { target: { value } });
    });

    expect(input.value).toBe(value);

    const form = renderResult.container.querySelector(
      "form"
    ) as HTMLFormElement;

    await act(async () => {
      fireEvent.submit(form);
    });

    renderResult.rerender(
      <ToDoList
        toDos={mockToDos}
        updateToDoDoneStatus={updateToDoDoneStatusMock}
        addToDo={addToDoMock}
      />
    );

    expect(renderResult.getByText(value)).toBeInTheDocument();
  });
});
