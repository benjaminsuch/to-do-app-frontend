import { FormEventHandler, useRef, useState, VFC } from "react";
import styled from "styled-components";
import { ToDo } from "../types/ToDo";

const Container = styled.div`
  width: 100%;
  padding: 24px;
  background: #eee;
  border-radius: 12px;
  box-shadow: #ccc 2px 2px 3px;
`;

const FormField = styled.div`
  display: flex;
`;

const Input = styled.input`
  flex: 1 1 auto;
`;

export interface ToDoNewFormProps {
  onSubmit: (name: ToDo["Name"]) => Promise<void> | void;
}

export const ToDoNewForm: VFC<ToDoNewFormProps> = ({ onSubmit }) => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
    event.preventDefault();
    setIsSubmitting(true);

    const data = new FormData(event.target as HTMLFormElement);
    const name = data.get("name");

    try {
      if (name && typeof name === "string") {
        await onSubmit(name);

        if (inputRef.current) {
          inputRef.current.value = "";
        }
      } else {
        // Show error?
      }
    } catch (error) {
      console.error(error);
    } finally {
      setIsSubmitting(false);
      inputRef.current?.focus();
    }
  };

  return (
    <Container id="todo-new-form">
      <form method="post" onSubmit={handleSubmit}>
        <FormField>
          <Input
            ref={inputRef}
            type="text"
            name="name"
            disabled={isSubmitting}
          />
          <button type="submit" disabled={isSubmitting}>
            Add
          </button>
        </FormField>
      </form>
    </Container>
  );
};
