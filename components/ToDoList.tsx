import { ToDo } from "../types/ToDo";
import ToDoEntry from "./ToDoEntry";
import { ToDoNewForm } from "./ToDoNewForm";

export interface ToDoListProps {
  addToDo: (name: ToDo["Name"]) => void;
  toDos: ToDo[];
  updateToDoDoneStatus: (id: number, done: boolean) => void;
}

export function ToDoList({
  addToDo,
  toDos,
  updateToDoDoneStatus,
}: ToDoListProps): JSX.Element {
  return (
    <div id="todo-list">
      {toDos
        .sort((t1, t2) => +(t1.Done || false) - +(t2.Done || false))
        .map((toDo) => (
          <ToDoEntry
            key={toDo.id}
            toDo={toDo}
            updateToDoDoneStatus={updateToDoDoneStatus}
          />
        ))}
      <ToDoNewForm onSubmit={addToDo} />
    </div>
  );
}
