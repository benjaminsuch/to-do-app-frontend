import { act, fireEvent, render, RenderResult } from "@testing-library/react";

import { mockToDos } from "../lib-test/mock-todos";
import { ToDoWidget } from "./ToDoWidget";

const toDoApi = {
  addToDo: jest.fn(),
  updateToDo: jest.fn(),
  getAllToDos: jest.fn(),
};

describe("ToDoWidget", () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(
      <ToDoWidget initialToDos={mockToDos} toDoApi={toDoApi} />
    );
  });

  it("should render all to-dos, the create-form and filter", () => {
    const { container } = renderResult;

    expect(container).toBeTruthy();
    expect(container.querySelector("#todo-filter")).toBeTruthy();
    expect(container.querySelector("#todo-list")).toBeTruthy();
    expect(container.querySelector("#todo-new-form")).toBeTruthy();
  });

  it("should not show todos that are open, when filter's 'open' is unchecked", () => {
    act(() => {
      renderResult.rerender(
        <ToDoWidget initialToDos={mockToDos} toDoApi={toDoApi} />
      );
    });

    const { container } = renderResult;
    const openCheckbox = container.querySelector(
      "#todo-filter input[type=checkbox][name=open]"
    ) as HTMLInputElement;

    expect(container.querySelectorAll("#todo-list > div").length).toBe(3);

    act(() => {
      fireEvent.click(openCheckbox);
    });

    expect(openCheckbox.checked).toEqual(false);
    expect(container.querySelectorAll("#todo-list > div").length).toBe(2);
  });

  it("should not show todos that are done, when filter's 'done' is unchecked", () => {
    act(() => {
      renderResult.rerender(
        <ToDoWidget initialToDos={mockToDos} toDoApi={toDoApi} />
      );
    });

    const { container } = renderResult;
    const doneCheckbox = renderResult.container.querySelector(
      "input[type=checkbox][name=done]"
    ) as HTMLInputElement;

    expect(container.querySelectorAll("#todo-list > div").length).toBe(3);

    act(() => {
      fireEvent.click(doneCheckbox);
    });

    expect(doneCheckbox.checked).toEqual(false);
    expect(container.querySelectorAll("#todo-list > div").length).toBe(2);
  });
});
