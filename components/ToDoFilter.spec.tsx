import { fireEvent, render, RenderResult } from "@testing-library/react";

import { ToDoFilter, ToDoFilterValues } from "./ToDoFilter";

describe("ToDoFilter", () => {
  let renderResult: RenderResult;
  let onChangeMock: (values: Partial<ToDoFilterValues>) => void;
  let openCheckbox: HTMLInputElement;
  let doneCheckbox: HTMLInputElement;
  let filter: ToDoFilterValues;

  beforeEach(() => {
    filter = { done: true, open: true };

    onChangeMock = jest
      .fn()
      .mockImplementation((values: Partial<ToDoFilterValues>) => {
        filter = { ...filter, ...values };
      });

    renderResult = render(
      <ToDoFilter
        open={filter.open}
        done={filter.done}
        onChange={onChangeMock}
      />
    );

    const { container } = renderResult;

    openCheckbox = container.querySelector(
      "input[type=checkbox][name=open]"
    ) as HTMLInputElement;
    doneCheckbox = container.querySelector(
      "input[type=checkbox][name=done]"
    ) as HTMLInputElement;
  });

  it("should render the filter", () => {
    expect(openCheckbox).toBeTruthy();
    expect(openCheckbox.parentElement?.textContent?.trim()).toBe("Open");
    expect(doneCheckbox).toBeTruthy();
    expect(doneCheckbox.parentElement?.textContent?.trim()).toBe("Done");
  });

  it("should update `filter.open` when clicking the 'Open' checkbox", () => {
    const oldValue = filter.open;

    fireEvent.click(openCheckbox);
    expect(onChangeMock).toHaveBeenCalled();
    expect(filter.open).toEqual(!oldValue);

    renderResult.rerender(
      <ToDoFilter
        open={filter.open}
        done={filter.done}
        onChange={onChangeMock}
      />
    );

    openCheckbox = renderResult.container.querySelector(
      "input[type=checkbox][name=open]"
    ) as HTMLInputElement;

    expect(openCheckbox.checked).toEqual(!oldValue);
  });

  it("should update `filter.done` when clicking the 'Done' checkbox", () => {
    const oldValue = filter.done;

    fireEvent.click(doneCheckbox);
    expect(onChangeMock).toHaveBeenCalled();
    expect(filter.done).toEqual(!oldValue);

    renderResult.rerender(
      <ToDoFilter
        open={filter.open}
        done={filter.done}
        onChange={onChangeMock}
      />
    );

    doneCheckbox = renderResult.container.querySelector(
      "input[type=checkbox][name=done]"
    ) as HTMLInputElement;

    expect(doneCheckbox.checked).toEqual(!oldValue);
  });
});
