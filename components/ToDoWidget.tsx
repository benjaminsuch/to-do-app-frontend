import { useState, VFC } from "react";

import { ToDoFilter, ToDoFilterValues } from "../components/ToDoFilter";
import { ToDoList } from "../components/ToDoList";
import { ToDo } from "../types/ToDo";
import { ToDoApi } from "../api/ToDoApi";

export interface ToDoWidgetProps {
  initialToDos: ToDo[];
  toDoApi: ToDoApi;
}

export const ToDoWidget: VFC<ToDoWidgetProps> = ({ initialToDos, toDoApi }) => {
  const [toDos, setToDos] = useState<ToDo[]>(initialToDos);
  const [filter, setFilter] = useState<ToDoFilterValues>({
    done: true,
    open: true,
  });

  async function updateToDoDoneStatus(
    id: number,
    done: boolean
  ): Promise<void> {
    const toDo: ToDo | undefined = toDos.find((toDo) => toDo.id === id);
    if (!toDo) {
      return;
    }

    const updatedToDo: ToDo = {
      ...toDo,
      Done: done,
    };

    setToDos((toDos) => [
      ...toDos.map((toDo) =>
        toDo.id === id && updatedToDo ? updatedToDo : toDo
      ),
    ]);
    await toDoApi.updateToDo(id, updatedToDo);
  }

  async function addToDo(Name: ToDo["Name"]) {
    const toDo = await toDoApi.addToDo({ Name });
    setToDos((toDos) => [...toDos, toDo]);
  }

  return (
    <>
      <h1>To-Dos</h1>
      <ToDoFilter
        done={filter.done}
        open={filter.open}
        onChange={(values: Partial<ToDoFilterValues>) =>
          setFilter({ ...filter, ...values })
        }
      />
      <ToDoList
        toDos={toDos.filter(
          (todo) =>
            (filter.done && Boolean(todo.Done) === true) ||
            (filter.open && Boolean(todo.Done) === false)
        )}
        updateToDoDoneStatus={updateToDoDoneStatus}
        addToDo={addToDo}
      />
    </>
  );
};
