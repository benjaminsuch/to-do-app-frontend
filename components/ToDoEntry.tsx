import { ChangeEvent } from "react";
import styled from "styled-components";

import { ToDo } from "../types/ToDo";

const ToDoWrapper = styled.div`
  width: 100%;
  padding: 24px;
  background: #eee;
  border-radius: 12px;
  box-shadow: #ccc 2px 2px 3px;
  margin-bottom: 24px;
`;

const Checkbox = styled.input`
  margin-right: 12px;
`;

interface ToDoEntryProps {
  toDo: ToDo;
  updateToDoDoneStatus: (id: number, done: boolean) => void;
}

export default function ToDoEntry({
  toDo: { id, Name, Done },
  updateToDoDoneStatus,
}: ToDoEntryProps): JSX.Element {
  return (
    <ToDoWrapper>
      <Checkbox
        type="checkbox"
        id={`${id}`}
        checked={Done}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          updateToDoDoneStatus(id, e.target.checked)
        }
      />
      <label htmlFor={`${id}`}>{Name}</label>
    </ToDoWrapper>
  );
}
