import { VFC } from "react";
import styled from "styled-components";

const Container = styled.div`
  padding-bottom: 24px;

  label + label {
    margin-left: 24px;
  }
`;

export interface ToDoFilterValues {
  done: boolean;
  open: boolean;
}

export interface ToDoFilterProps {
  done: ToDoFilterValues["done"];
  open: ToDoFilterValues["open"];
  onChange: (values: Partial<ToDoFilterValues>) => void;
}

export const ToDoFilter: VFC<ToDoFilterProps> = ({ done, open, onChange }) => {
  return (
    <Container id="todo-filter">
      <label>
        <input
          type="checkbox"
          checked={open}
          onChange={() => onChange({ open: !open })}
          name="open"
        />{" "}
        Open
      </label>
      <label>
        <input
          type="checkbox"
          checked={done}
          onChange={() => onChange({ done: !done })}
          name="done"
        />{" "}
        Done
      </label>
    </Container>
  );
};
