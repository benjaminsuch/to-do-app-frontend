import { act, fireEvent, render, RenderResult } from "@testing-library/react";

import { ToDoNewForm, ToDoNewFormProps } from "./ToDoNewForm";

describe("ToDoNewForm", () => {
  let renderResult: RenderResult;
  let input: HTMLInputElement;
  let addToDoMock: ToDoNewFormProps["onSubmit"];
  let button: HTMLButtonElement;

  beforeEach(async () => {
    addToDoMock = jest.fn();
    renderResult = render(<ToDoNewForm onSubmit={addToDoMock} />);

    input = renderResult.container.querySelector(
      'input[name="name"]'
    ) as HTMLInputElement;

    button = renderResult.container.querySelector(
      'button[type="submit"]'
    ) as HTMLButtonElement;
  });

  it("should render create todo form", () => {
    const { container } = renderResult;

    expect(container).toBeTruthy();
    expect(input).toBeTruthy();
    expect(button).toBeTruthy();
  });

  it("should disable all form elements while submitting", async () => {
    const value = "my todo";

    act(() => {
      renderResult.rerender(<ToDoNewForm onSubmit={addToDoMock} />);
    });

    act(() => {
      fireEvent.change(input, { target: { value } });
    });

    expect(input.value).toBe(value);

    const form = renderResult.container.querySelector(
      "form"
    ) as HTMLFormElement;

    await act(async () => {
      fireEvent.submit(form);
    });

    expect(addToDoMock).toHaveBeenCalled();

    //TODO: Check "disabled" attribute during submission
  });
});
